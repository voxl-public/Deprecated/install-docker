#!/bin/bash

source docker-install/package-names.sh

version_number=`cat docker-install/version.txt`
archive_name=docker-install_$version_number.tar.gz

rm -f $archive_name

cd docker-install
rm -f *.ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$curses_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$smartcols_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$systemd_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$losetup_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$swaponoff_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$umount_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$util_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$aufs_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$cgroup_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$cgroup_lite_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$docker_ipk
cd ..

echo "Creating tar file for docker installer version $version_number"
tar -czvf $archive_name docker-install/
